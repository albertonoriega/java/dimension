
package dimension;

public class DimensionApp {

   
    public static void main(String[] args) {
        
        Dimension ladrillo, caja;
        ladrillo= new Dimension();
        
        ladrillo.setAlto(2);
        System.out.println(ladrillo.getAlto());
        System.out.println(ladrillo.getAncho());
        System.out.println(ladrillo.getProfundidad());
        
        caja = new Dimension(5, 2, 6);
        
        System.out.println(caja.getAlto());
        System.out.println(caja.getAncho());
        System.out.println(caja.getProfundidad());
        
        System.out.println(ladrillo.getVolumen());
        System.out.println(caja.getVolumen());
    }
    
}
