
package dimension;

public class Dimension {
    
    public double alto;
    public double ancho;
    public double profundidad;

    public Dimension() {
        this.alto = 1;
        this.ancho = 2;
        this.profundidad = 3;
    }

    public Dimension(double alto, double ancho, double profundidad) {
        this.alto = alto;
        this.ancho = ancho;
        this.profundidad = profundidad;
    }

    
    
    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getProfundidad() {
        return profundidad;
    }

    public void setProfundidad(double profundidad) {
        this.profundidad = profundidad;
    }
    
    public double getVolumen(){
        
         double volumen;   
         volumen = this.alto*this.ancho*this.profundidad;
         return volumen;
    }
    
}
